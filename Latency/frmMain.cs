﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Latency
{
    public partial class frmMain : Form
    {


        cFunction Function = new cFunction();
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            txtDLLPath.Text = Application.StartupPath + "\\sample.dll";
            labelStatus.ForeColor = Color.PaleVioletRed;
            labelStatus.Text = "Status : Waiting for start";
        }

        private void btnToggle_Click(object sender, EventArgs e)
        {
            if (btnToggle.Text == "Start")
            {
                if (!System.IO.File.Exists(txtDLLPath.Text))
                {
                    MessageBox.Show("DLL file not found. Please select new DLL file");
                    return;
                }
                txtProcessName.ReadOnly = true;
                btnBrowse.Enabled = false;
                btnToggle.Text = "Stop";
                labelStatus.ForeColor = Color.Fuchsia;
                labelStatus.Text = "Status : Waiting for process...";
                timerInjection.Enabled = true;
                timerInjection.Start();
            }
            else
            {
                txtProcessName.ReadOnly = false;
                btnBrowse.Enabled = true;
                btnToggle.Text = "Start";
                labelStatus.ForeColor = Color.PaleVioletRed;
                labelStatus.Text = "Status : Waiting for start";
                timerInjection.Stop();
                timerInjection.Enabled = false;
            }
        }

        private string SelectDLLFile(string initialDirectory)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter =
               "DLL files (*.dll)|*.dll|All files (*.*)|*.*";
            dialog.InitialDirectory = initialDirectory;
            dialog.Title = "Select a DLL file";
            return (dialog.ShowDialog() == DialogResult.OK)
               ? dialog.FileName : null;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            txtDLLPath.Text = SelectDLLFile(Application.StartupPath);
        }

        private void timerInjection_Tick(object sender, EventArgs e)
        {
            Application.DoEvents();
            uint proID = Function.GetProcessIDFunc(txtProcessName.Text);
            string dllPathFile = txtDLLPath.Text;
            if (proID != 0)
            {
                try
                {
                    if (Function.InjectDLLFunc(proID, dllPathFile))
                    {
                        timerInjection.Enabled = false;
                        labelStatus.ForeColor = Color.Green;
                        labelStatus.Text = "Status : Injected!!!";
                        txtProcessName.ReadOnly = false;
                        btnBrowse.Enabled = true;
                        btnToggle.Text = "Start";
                        timerInjection.Stop();
                        timerInjection.Enabled = false;
                        if (checkBoxAutoClose.Checked) Application.Exit();
                    }
                }
                catch
                {

                }
            }
        }
    }
}
