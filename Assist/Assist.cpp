/*
		nProtect.dll Function Library
		Copyright (C) 2008 Asnpnet Software
		Open Source For Lernning and Deloping Only
*/

#include <Windows.h>
#include <tlhelp32.h>
#include "Data.h"
#include "Assist.h"

#define TIMER1		2000


HMODULE	dllHandle;
HWND AuHwnd;
UINT AntiInjectTh(LPVOID lpParameter);
VOID CALLBACK TimerF9(HWND hwnd,UINT message,UINT idTimer,DWORD dwTime);

BOOL APIENTRY DllMain(HANDLE hModule,DWORD  ul_reason_for_call,LPVOID lpReserved)
{
    return TRUE;
}

//For Freeze BPM Ball -> Address is 0x568CDB
NPAPI BOOL WriteProcessBytes(HANDLE hProcess, LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize)
{
	DWORD dwOldProtect; 
	BOOL boolReturn = FALSE; 

	if(hProcess == NULL) 
	{
		VirtualProtect(lpBaseAddress, nSize, PAGE_EXECUTE_READWRITE, &dwOldProtect); 
		boolReturn = ((memcpy(lpBaseAddress, lpBuffer, nSize))? 1 : 0); 
		VirtualProtect(lpBaseAddress, nSize, dwOldProtect, &dwOldProtect); 
	} 
	else 
	{ 
		VirtualProtectEx(hProcess, lpBaseAddress, nSize, PAGE_EXECUTE_READWRITE, &dwOldProtect); 
		boolReturn = WriteProcessMemory(hProcess, lpBaseAddress, (LPVOID)lpBuffer, nSize, 0); 
		VirtualProtectEx(hProcess, lpBaseAddress, nSize, dwOldProtect, &dwOldProtect); 
	} 

	VirtualFreeEx(hProcess, lpBaseAddress, nSize, MEM_RELEASE);

	return boolReturn;
}

NPAPI BOOL TransferData(DWORD dwMsg, const _TCHAR *Buffer, DWORD dwBytes,HWND hServer) 
{
	BOOL bSend;
	COPYDATASTRUCT cpStructData;
	LPWM_DATASTRUCTURE lpMsg;
	cpStructData.cbData = dwBytes + _WM_HEADER_SIZE;
	lpMsg = (LPWM_DATASTRUCTURE)LocalAlloc(LPTR,cpStructData.cbData);
	lpMsg->hClient = NULL;
	lpMsg->iMessage = dwMsg;
	lpMsg->cbSize = dwBytes;
	cpStructData.lpData = lpMsg;
	if(Buffer!=NULL)
	{
		 SETSTRLEN(lpMsg->Data,dwBytes);
		 MEMCPY(lpMsg->Data,Buffer,dwBytes); 
	}
	bSend = SendMessage(hServer,WM_COPYDATA,(WPARAM)hServer,(LPARAM)&cpStructData);
	return(bSend);
}

NPAPI DWORD InjectDLL(DWORD dwPID, LPCSTR strHookDLL)
{
	DWORD dwAttr = GetFileAttributes(strHookDLL);
	if(dwAttr == 0xFFFFFFFF)
	{
		DWORD dwError = GetLastError();

		switch(dwError)
		{
			case ERROR_FILE_NOT_FOUND:
				return 1;
			break;
			
			case ERROR_PATH_NOT_FOUND:
				return 2;
			break;
			
			case ERROR_ACCESS_DENIED:
				return 3;
			break;

			default:
				return 4;
			break;
		}
	}
	else
	{
		if(dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		{
			return 5;
		}
	}

	if(dwPID == NULL)
	{
		return 6;
	}

	HANDLE hProcess;								//** Will be the process we inject
	HMODULE hKernel;								//** Will hold Kernel module
	LPVOID lpExecString, lpLoadLibraryAddr;			//** Remote string and LoadLibrary() address holder

	hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPID); //** Attempt to gain access to user-defined process

	if(hProcess == INVALID_HANDLE_VALUE) //** Error
	{
		return 7;
	}

	hKernel = LoadLibrary("kernel32.dll");	//** Attempt to load the kernel

	if(hKernel == NULL)
	{
		CloseHandle(hProcess);
		return 8;
	}

	lpLoadLibraryAddr = (LPVOID)GetProcAddress(hKernel, "LoadLibraryA");	//** Attempt to get LoadLibrary address

	lpExecString = (LPVOID)VirtualAllocEx(hProcess, NULL, strlen(strHookDLL), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE); 

	if(WriteProcessBytes(hProcess, (LPVOID)lpExecString, strHookDLL, strlen(strHookDLL)) == FALSE)
	{
		CloseHandle(hProcess);

		return 9;
	}
	
	HANDLE hRemoteThread = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)lpLoadLibraryAddr, (LPVOID)lpExecString, NULL, NULL);

	if(hRemoteThread == INVALID_HANDLE_VALUE)
	{
		CloseHandle(hRemoteThread);
		CloseHandle(hProcess);
		return 10;
	}

	CloseHandle(hProcess);

	return 0;
}

NPAPI DWORD GetProcessID(LPCSTR strProcessName)
{
	if (strProcessName == NULL){
		MessageBox(GetActiveWindow(),"�ʹ��Ҵ ��س��к� Process Name","Error",MB_ICONERROR);
		return FALSE;
	}
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;

	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if(hProcessSnap == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	else
	{	
		pe32.dwSize = sizeof(PROCESSENTRY32);

		if(Process32First(hProcessSnap, &pe32) == NULL)
		{
			CloseHandle(hProcessSnap);
			return FALSE;
		}
		else
		{
			do
			{
				if(strcmp(pe32.szExeFile, strProcessName) == 0)
				{
					CloseHandle(hProcessSnap);
					return pe32.th32ProcessID;	//** Return Process ID	
				}

			} while(Process32Next(hProcessSnap, &pe32));
		}
	}

	CloseHandle(hProcessSnap);

	return FALSE; //** If we make it here, error process not found
}

NPAPI DWORD GetScreenPixelColor(int X,int Y)
{
	HDC TargethDC;
	HWND TargetHwnd;
	DWORD ColorR;
	
	TargetHwnd = GetDesktopWindow();
	TargethDC = GetWindowDC(TargetHwnd);

	ColorR = GetPixel(TargethDC,X,Y);
	
	ReleaseDC(TargetHwnd,TargethDC);

	return (DWORD)ColorR;
}


NPAPI DWORD KillProcess(DWORD ProcessID)
{
	HANDLE ProcHan;
	ProcHan = OpenProcess(PROCESS_ALL_ACCESS,FALSE,ProcessID);

	if(ProcHan==NULL)
	{
		return FALSE;
	}

	TerminateProcess(ProcHan,0);
	return TRUE;
}


NPAPI BOOL ChangeResolution(int Width,int Height)
{
	DEVMODE dmSettings;

	memset(&dmSettings,0,sizeof(dmSettings));

	if (!EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dmSettings)) return false;

	dmSettings.dmPelsWidth=Width;
	dmSettings.dmPelsHeight=Height;
	dmSettings.dmFields=DM_PELSWIDTH | DM_PELSHEIGHT;

	int result=ChangeDisplaySettings(&dmSettings,CDS_FULLSCREEN);
	if (result!=DISP_CHANGE_SUCCESSFUL) return false;

	return true;
}

NPAPI DWORD GetXResolution()
{
	DEVMODE dmSettings;
	memset(&dmSettings,0,sizeof(dmSettings));

	if (!EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dmSettings))
	{
		MessageBox(NULL,"�������ö��ҹ��õ�駤�ҡ��촨�","MyFunction DLL",MB_OK);
		return 0;
	}

	return dmSettings.dmPelsWidth;
}
NPAPI DWORD GetYResolution()
{
	DEVMODE dmSettings;
	memset(&dmSettings,0,sizeof(dmSettings));

	if (!EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dmSettings))
	{
		MessageBox(NULL,"�������ö��ҹ��õ�駤�ҡ��촨�","MyFunction DLL",MB_OK);
		return 0;
	}

	return dmSettings.dmPelsHeight;
}


NPAPI void HideModule(HINSTANCE hModule)
{
	DWORD dwPEB_LDR_DATA = 0;
	_asm
	{
		pushad;
		pushfd;
		mov eax, fs:[30h]		   // PEB
		mov eax, [eax+0Ch]		  // PEB->ProcessModuleInfo
		mov dwPEB_LDR_DATA, eax	 // Save ProcessModuleInfo

InLoadOrderModuleList:
		mov esi, [eax+0Ch]					  // ProcessModuleInfo->InLoadOrderModuleList[FORWARD]
		mov edx, [eax+10h]					  //  ProcessModuleInfo->InLoadOrderModuleList[BACKWARD]

		LoopInLoadOrderModuleList: 
		    lodsd							   //  Load First Module
			mov esi, eax		    			//  ESI points to Next Module
			mov ecx, [eax+18h]		    		//  LDR_MODULE->BaseAddress
			cmp ecx, hModule		    		//  Is it Our Module ?
			jne SkipA		    		    	//  If Not, Next Please (@f jumps to nearest Unamed Lable @@:)
		    	mov ebx, [eax]				  //  [FORWARD] Module 
		    	mov ecx, [eax+4]    		    	//  [BACKWARD] Module
		    	mov [ecx], ebx				  //  Previous Module's [FORWARD] Notation, Points to us, Replace it with, Module++
		    	mov [ebx+4], ecx			    //  Next Modules, [BACKWARD] Notation, Points to us, Replace it with, Module--
			jmp InMemoryOrderModuleList		//  Hidden, so Move onto Next Set
		SkipA:
			cmp edx, esi					    //  Reached End of Modules ?
			jne LoopInLoadOrderModuleList		//  If Not, Re Loop

InMemoryOrderModuleList:
		mov eax, dwPEB_LDR_DATA		  //  PEB->ProcessModuleInfo
		mov esi, [eax+14h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[START]
		mov edx, [eax+18h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[FINISH]

		LoopInMemoryOrderModuleList: 
			lodsd
			mov esi, eax
			mov ecx, [eax+10h]
			cmp ecx, hModule
			jne SkipB
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp InInitializationOrderModuleList
		SkipB:
			cmp edx, esi
			jne LoopInMemoryOrderModuleList

InInitializationOrderModuleList:
		mov eax, dwPEB_LDR_DATA				    //  PEB->ProcessModuleInfo
		mov esi, [eax+1Ch]						 //  ProcessModuleInfo->InInitializationOrderModuleList[START]
		mov edx, [eax+20h]						 //  ProcessModuleInfo->InInitializationOrderModuleList[FINISH]

		LoopInInitializationOrderModuleList: 
			lodsd
			mov esi, eax		
			mov ecx, [eax+08h]
			cmp ecx, hModule		
			jne SkipC
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp Finished
		SkipC:
			cmp edx, esi
			jne LoopInInitializationOrderModuleList

		Finished:
			popfd;
			popad;
	}
}

NPAPI void SetF9Timer(UINT Interval,HWND hWnd)
{
	SetTimer(hWnd,TIMER1,Interval,TimerF9);
}

NPAPI void CloseF9Timer(UINT Interval,HWND hWnd)
{
	KillTimer(hWnd,TIMER1);
}

VOID CALLBACK TimerF9(HWND hwnd,UINT message,UINT idTimer,DWORD dwTime)
{
	SendMessage(FindWindow("DLightClass",NULL),WM_KEYDOWN,VK_F9,NULL);
}