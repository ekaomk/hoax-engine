//My nProtect's InterProcess Communication Variable Define

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <tchar.h>

#ifndef __WMCOPYHDRS__
#define __WMCOPYHDRS__

#define	SETSTRLEN(szString,iNum)	((szString)[(iNum)]='\0')
#define	FINDWINDOW(szWndClassName,szWndTitle)	(FindWindow((LPCTSTR)szWndClassName,(LPCTSTR)szWndTitle))
#define STPRINTF(szBuffer,szFormat,szArgs)		(_stprintf((_TCHAR*)szBuffer,(_TCHAR*)szFormat,(unsigned long)szArgs))
#define STRCPY(strDest,strSource,iCount)		(_tcsncpy((_TCHAR*)strDest,(_TCHAR*)strSource,size_t(iCount)))
#define STRLEN(szValue)							(_tcslen((const _TCHAR*)szValue))
#define TPRINTF(szValue)						(_tprintf((_TCHAR*)szValue))
#define MEMCPY(szDest,szTarget,iCount)			(memcpy((void *) szDest,(const void*)szTarget,(size_t)iCount))
#define TCMP(szDest,szTarget)					(_tcsicmp((const _TCHAR*)szDest,(const _TCHAR*)szTarget))
#define	_WM_MAXMESSAGE_SIZE		0x10000
#define _WM_HEADER_SIZE	(2*sizeof(DWORD)+sizeof(HWND))

typedef struct _WM_DATASTRUCTURE{
	DWORD cbSize;
	DWORD iMessage;
	HWND hClient;
	_TCHAR Data[_WM_MAXMESSAGE_SIZE - _WM_HEADER_SIZE];	
}WM_DATASTRUCTURE,*LPWM_DATASTRUCTURE;

#endif __WMCOPYHDRS__

#define GX_EngineData	164110
