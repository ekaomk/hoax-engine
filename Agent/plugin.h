//Audition Agent PluginLoader by nProtect

#define MYDEF(f) (WINAPI *f)
HINSTANCE	dllHandle;

BOOL MYDEF(PluginMain)(HWND AuHandle,HWND AgentHandle,DWORD Message,LPCSTR Parameter);

void LoadPlugin(LPCSTR PluginFile)
{
	dllHandle = LoadLibrary(PluginFile);
	    if(dllHandle == NULL)
        {
            MessageBox(0, "Plugin Loader Error", "Error", MB_ICONSTOP);
        }

	#define LOADDLL(f) *((void**)&f)=GetProcAddress(dllHandle,#f)
		LOADDLL(PluginMain);
}