// Defines the different messages and the data structure which is 
// to be transferred between the various processes.

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <windows.h>
#include <tchar.h>

#ifndef __WMCOPYHDRS__
#define __WMCOPYHDRS__

#define	SETSTRLEN(szString,iNum)	((szString)[(iNum)]='\0')
#define	FINDWINDOW(szWndClassName,szWndTitle)	(FindWindow((LPCTSTR)szWndClassName,(LPCTSTR)szWndTitle))
#define STPRINTF(szBuffer,szFormat,szArgs)		(_stprintf((_TCHAR*)szBuffer,(_TCHAR*)szFormat,(unsigned long)szArgs))
#define STRCPY(strDest,strSource,iCount)		(_tcsncpy((_TCHAR*)strDest,(_TCHAR*)strSource,size_t(iCount)))
#define STRLEN(szValue)							(_tcslen((const _TCHAR*)szValue))
#define TPRINTF(szValue)						(_tprintf((_TCHAR*)szValue))
#define MEMCPY(szDest,szTarget,iCount)			(memcpy((void *) szDest,(const void*)szTarget,(size_t)iCount))
#define TCMP(szDest,szTarget)					(_tcsicmp((const _TCHAR*)szDest,(const _TCHAR*)szTarget))
#define	_WM_MAXMESSAGE_SIZE		0x10000
#define _WM_HEADER_SIZE	(2*sizeof(DWORD)+sizeof(HWND))

typedef struct _WM_DATASTRUCTURE{
	DWORD cbSize;
	DWORD iMessage;
	HWND hClient;
	_TCHAR Data[_WM_MAXMESSAGE_SIZE - _WM_HEADER_SIZE];	
}WM_DATASTRUCTURE,*LPWM_DATASTRUCTURE;

#endif 

/*Define Command Here__WMCOPYHDRS__
Sample: #define Command Name	Command Number (4 digit only)
#define COMMAND_NAME	1000 (not begin number 0)

And goto AuditionDebug.cpp search WinProc function goto WM_COPYDATA and add
case command and your code

case COMMAND_NAME:
	your code here
	break;
*/
#define TEST_MSG			5401
#define UNLOAD_HSD			4150
#define BYPASS_ALTTAB		3701
#define DISABLE_HSD_API		2571
#define FAKE_DISCONNECT		8870
#define EXIT_GAME			4152
#define SPAM_F9				5704
#define PER_SCANO			3950
#define AUTO_SPACE			1874
#define TEST_LOCK			5517
#define SHOW_PARAM			9042
#define LOAD_PLUGIN			1741
#define SEND_PLUGIN			1742
#define LOAD_DLL			3487
#define CH2WIN				1671
#define TEST_KEY			1742
#define SHOWCREDITS			5498
#define PVCODE				6871
#define RELOCK				7811
#define INT_PERSPAM			8197
#define DIS_PERSPAM			8198

#define WRITE_NOP			1658
#define WRITE_XOR			1659

#define NORMAL_HACK			6741
#define BEATUP_HACK			6742
#define BEAT_COMBO_HACK	6743

//======FUNNY HACK=======
#define CASH_HACK		9711
#define CHAIN_HACK		9712