#include <windows.h>

BOOL bWantsExit = FALSE;

DWORD SetDebugFlag()
{
   while(!bWantsExit)
   {
      _asm
      {
		 //Reset basic IsDebuggerPresent
         mov eax, dword ptr fs:[0x18]
         mov eax, dword ptr ds:[eax+0x30]
         mov byte ptr ds:[eax+0x2], 0x0
      }
      Sleep(10);
   }
   return 0;
}

void HideModule(HINSTANCE hModule)
{
	DWORD dwPEB_LDR_DATA = 0;
	_asm
	{
		pushad;
		pushfd;
		mov eax, fs:[30h]		   // PEB
		mov eax, [eax+0Ch]		  // PEB->ProcessModuleInfo
		mov dwPEB_LDR_DATA, eax	 // Save ProcessModuleInfo

InLoadOrderModuleList:
		mov esi, [eax+0Ch]					  // ProcessModuleInfo->InLoadOrderModuleList[FORWARD]
		mov edx, [eax+10h]					  //  ProcessModuleInfo->InLoadOrderModuleList[BACKWARD]

		LoopInLoadOrderModuleList: 
		    lodsd							   //  Load First Module
			mov esi, eax		    			//  ESI points to Next Module
			mov ecx, [eax+18h]		    		//  LDR_MODULE->BaseAddress
			cmp ecx, hModule		    		//  Is it Our Module ?
			jne SkipA		    		    	//  If Not, Next Please (@f jumps to nearest Unamed Lable @@:)
		    	mov ebx, [eax]				  //  [FORWARD] Module 
		    	mov ecx, [eax+4]    		    	//  [BACKWARD] Module
		    	mov [ecx], ebx				  //  Previous Module's [FORWARD] Notation, Points to us, Replace it with, Module++
		    	mov [ebx+4], ecx			    //  Next Modules, [BACKWARD] Notation, Points to us, Replace it with, Module--
			jmp InMemoryOrderModuleList		//  Hidden, so Move onto Next Set
		SkipA:
			cmp edx, esi					    //  Reached End of Modules ?
			jne LoopInLoadOrderModuleList		//  If Not, Re Loop

InMemoryOrderModuleList:
		mov eax, dwPEB_LDR_DATA		  //  PEB->ProcessModuleInfo
		mov esi, [eax+14h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[START]
		mov edx, [eax+18h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[FINISH]

		LoopInMemoryOrderModuleList: 
			lodsd
			mov esi, eax
			mov ecx, [eax+10h]
			cmp ecx, hModule
			jne SkipB
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp InInitializationOrderModuleList
		SkipB:
			cmp edx, esi
			jne LoopInMemoryOrderModuleList

InInitializationOrderModuleList:
		mov eax, dwPEB_LDR_DATA				    //  PEB->ProcessModuleInfo
		mov esi, [eax+1Ch]						 //  ProcessModuleInfo->InInitializationOrderModuleList[START]
		mov edx, [eax+20h]						 //  ProcessModuleInfo->InInitializationOrderModuleList[FINISH]

		LoopInInitializationOrderModuleList: 
			lodsd
			mov esi, eax		
			mov ecx, [eax+08h]
			cmp ecx, hModule		
			jne SkipC
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp Finished
		SkipC:
			cmp edx, esi
			jne LoopInInitializationOrderModuleList

		Finished:
			popfd;
			popad;
	}
}