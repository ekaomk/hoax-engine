﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictator
{
    public partial class frmMain : Form
    {
        cFunction Function = new cFunction();
        Int32 hAgent = 0;

        string ProcessName = "Test.exe";
        string AgentPath = Application.StartupPath + "\\Agent.dll\0";
        string AgentClassName = "CHoaxAgent";

        public frmMain()
        {
            InitializeComponent();
        }

        private void timerChkProcess_Tick(object sender, EventArgs e)
        {
            Application.DoEvents();
            uint proID = Function.GetProcessIDFunc(ProcessName);
            string dllPathFile = AgentPath;
            if (proID != 0)
            {
                if (hAgent == 0)
                {
                    
                    try
                    {
                        if (Function.InjectDLLFunc(proID, dllPathFile))
                        {
                            timerInjection.Enabled = false;
                            labelStatus.ForeColor = Color.Green;
                            labelStatus.Text = "Status : Injected and running";
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            timerInjection.Enabled = true;
            timerInjection.Start();

            timerChkAgent.Enabled = true;
            timerChkAgent.Start();

            labelStatus.ForeColor = Color.Red;
            labelStatus.Text = "Status : Waiting for injection...";
        }

        private void timerChkAgent_Tick(object sender, EventArgs e)
        {

            hAgent = Function.FindWindowFunc(AgentClassName, null);
            if (hAgent == 0 && !timerInjection.Enabled)
            {
                timerInjection.Enabled = true; 
                timerInjection.Start();
                labelStatus.ForeColor = Color.Red;
                labelStatus.Text = "Status : Waiting for injection...";
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Function.TransferDataFunc(5401, "tmp", 3, (IntPtr)hAgent); //Unload HS
        }
    }
}
