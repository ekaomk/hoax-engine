﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Dictator
{
    class cFunction
    {
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern Int32 FindWindow(String lpClassName, String lpWindowName);

        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int TransferData(uint dwMsg, string Buffer, uint dwBytes, IntPtr hServer);
        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int WriteProcessBytes(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, uint nSize);
        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint InjectDLL(uint dwPID, string strHookDLL);
        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint GetProcessID(string strProcessName);
        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint KillProcess(uint ProcessID);
        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint ChangeResolution(int Width, int Height);
        [DllImport("Assist.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void HideModule(IntPtr hModule);

        public int TransferDataFunc(uint dwMsg, string Buffer, uint dwBytes, IntPtr hServer)
        {
            return TransferData(dwMsg, Buffer, dwBytes, hServer);
        }

        public Int32 FindWindowFunc(String lpClassName, String lpWindowName)
        {
            return FindWindow(lpClassName, lpWindowName);
        }

        public bool InjectDLLFunc(uint ProcessID,string DLLPath)
        {
            if (InjectDLL(ProcessID, DLLPath) == 0) return true;
            return false;
        }

        public uint GetProcessIDFunc(string ProcessName)
        {
            return GetProcessID(ProcessName);
        }
    }
}
